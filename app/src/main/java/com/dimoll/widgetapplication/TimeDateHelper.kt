package com.dimoll.widgetapplication

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object TimeDateHelper {

    fun getTime(): String {
        @SuppressLint("SimpleDateFormat")
        val simpleDateFormat = SimpleDateFormat("HH:mm:ss")
        return simpleDateFormat.format(Date())
    }

    fun getDate(): String {
        @SuppressLint("SimpleDateFormat")
        val simpleDateFormat = SimpleDateFormat("EEE, d MMM, ''yy")
        return simpleDateFormat.format(Date())
    }
}
