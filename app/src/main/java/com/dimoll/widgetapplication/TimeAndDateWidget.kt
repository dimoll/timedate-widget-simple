package com.dimoll.widgetapplication

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.util.Log
import android.widget.RemoteViews

class TimeAndDateWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        Log.d("TimeAndDateWidget", "onUpdate!")

        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
            Log.d("TimeAndDateWidget", "onUpdateFOR!")

        }
    }

    companion object {

        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            Thread {
                while (true) {
                    val widgetTime = TimeDateHelper.getTime()
                    val widgetDate = TimeDateHelper.getDate()
                    // Construct the RemoteViews object
                    val views = RemoteViews(context.packageName, R.layout.time_and_date_widget)
                    views.setTextViewText(R.id.app_widget_time, widgetTime)
                    views.setTextViewText(R.id.app_widget_date, widgetDate)

                    Log.d("TimeAndDateWidget", "updateAppWidget!")
                    // Instruct the widget manager to update the widget
                    appWidgetManager.updateAppWidget(appWidgetId, views)
                    Thread.sleep(1000)
                }

            }.start()
        }
    }
}

